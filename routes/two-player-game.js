/**
 * Created by gabriel on 12/8/16.
 */

var oracle = require('oracledb');
oracle.autoCommit = true;
var mongodb = require('mongodb');
var player1Name = "";
var player2Name = "";
var difficulty = "easy";
var players = 2;
var queryFactory = require('../public/javascripts/giveQuery');

var jsdom = require("jsdom").jsdom;
jsdom.env("", function(err, window) {
    if (err) {
        console.error(err);
        return;
    }
    global.$ = require("jquery")(window);
});

oracle.createPool(
    {
        user: 'cswords',
        password: 'cis550db',
        connectString: 'cis550project8.cq7htkvwvgeh.us-west-2.rds.amazonaws.com:1521/TRIVIA_A'
    }
);

/* GET home page. */

exports.do_work= function(req, res) {
    if (req.query.difficulty != "") {
        difficulty = req.query.difficulty;
    }
    player1Name = req.query.first_player;
    player2Name = req.query.second_player;
    res.render('two-player-game.jade',
        {   title: "Two Player",
            playerOne: req.query.first_player,
            playerTwo: req.query.second_player }
    );
};

exports.db_query=function(req, res) {

    queryFactory.giveQuery("easy", oracle, mongodb.MongoClient, res);
};

exports.setHighScores = function(req, res, next) {
    var oracle = require('oracledb');

    oracle.createPool(
        {
            user: 'cswords',
            password: 'cis550db',
            connectString: 'cis550project8.cq7htkvwvgeh.us-west-2.rds.amazonaws.com:1521/TRIVIA_A'
        }
    );
    //get date and time for primary key
    var d = new Date();
    var t = d.getTime();

    var query = "INSERT INTO high_scores VALUES (" + t + ", " + "'" + player1Name + "'"
                            + ", " + req.query.p1score  + ")";
    console.log(query);
    t = t + 1;

    //build query
    query2 = "INSERT INTO high_scores VALUES (" + t + ", " + "'" + player2Name  + "'"
        + ", " + req.query.p2score + ")";
    console.log(query2);


    //open connection to add high scores
    oracle.getConnection(function(err, conn) {
                conn.execute(query, [], function(err, results) {
                    if (err) {console.error(err.message); return;}
                        //get a new time for primary key

                        //execute
                    conn.execute(query2, [], function(err, results) {
                        if (err) {console.error(err.message); return;}
                        conn.close();

                    });
                    //if it's one player, close the connection
                });
    });



}