var express = require('express');
var router = express.Router();
var firstScore = 100;

/* GET home page. */
router.get('/', function(req, res, next) {
    var oracle = require('oracledb');
	oracle.createPool(
	    {
	        user: 'cswords',
	        password: 'cis550db',
	        connectString: 'cis550project8.cq7htkvwvgeh.us-west-2.rds.amazonaws.com:1521/TRIVIA_A'
	    }
	);
	var query = "SELECT * FROM (SELECT username, score FROM high_scores ORDER BY score desc) WHERE ROWNUM <= 10";

	oracle.getConnection(function(err, conn) {
				if (err) {console.error(err.message); return;}
	            conn.execute(query, [], function(err, results) {
	                if (err) {console.error(err.message); return;}
	                res.render('index', { title: 'Trivia Wars', results: results.rows });
	                conn.close();
	            });
 	});
});



module.exports = router;
