/**
 * Created by gabriel on 12/8/16.
 */


var oracle = require('oracledb');
oracle.autoCommit = true;
var mongodb = require('mongodb');
var difficulty = "easy";
var player1Name = "";

var queryFactory = require('../public/javascripts/giveQuery');

var jsdom = require("jsdom").jsdom;
jsdom.env("", function(err, window) {
    if (err) {
        console.error(err);
        return;
    }
    global.$ = require("jquery")(window);
});

oracle.createPool(
    {
        user: 'cswords',
        password: 'cis550db',
        connectString: 'cis550project8.cq7htkvwvgeh.us-west-2.rds.amazonaws.com:1521/TRIVIA_A'
    }
);

/* GET home page. */

exports.do_work= function(req, res) {
    if (req.query.difficulty != "") {
        difficulty = req.query.difficulty;
    }
    player1Name = req.query.first_player;
    res.render('single-player-game.jade',
        { 	title: "Single Player",
            playerName: req.query.first_player }
    );
};

exports.setHighScores = function(req, res, next) {
    var oracle = require('oracledb');

    oracle.createPool(
        {
            user: 'cswords',
            password: 'cis550db',
            connectString: 'cis550project8.cq7htkvwvgeh.us-west-2.rds.amazonaws.com:1521/TRIVIA_A'
        }
    );
    //get date and time for primary key
    var d = new Date();
    var t = d.getTime();

    var query = "INSERT INTO high_scores VALUES (" + t + ", " + "'" + player1Name + "'"
                            + ", " + parseInt(req.query.p1score) + ")";

    //open connection to add high scores
    oracle.getConnection(function(err, conn) {
                conn.execute(query, [], function(err, results) {
                    if (err) {console.error(err.message); return;}
                    conn.close();
                });
    });



}

// 1. question 2. correct 3-5 wrong answers