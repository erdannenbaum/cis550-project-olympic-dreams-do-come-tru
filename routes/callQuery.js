
exports.giveQuery = function(req, res) {
	var oracle = require('oracledb');
	oracle.autoCommit = true;
	var mongodb = require('mongodb');
	var difficulty = req.query.difficulty

	var queryFactory = require('../public/javascripts/giveQuery');

	oracle.createPool(
	    {
	        user: 'cswords',
	        password: 'cis550db',
	        connectString: 'cis550project8.cq7htkvwvgeh.us-west-2.rds.amazonaws.com:1521/TRIVIA_A'
	    }
	);
	queryFactory.giveQuery(difficulty, oracle, mongodb.MongoClient, res);
}