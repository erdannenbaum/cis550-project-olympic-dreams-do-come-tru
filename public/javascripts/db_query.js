/**
 * Created by gabriel on 12/12/16.
 */

var counter = 0;
var p1Score = 0;
var p2Score = 0;
var p1Timer;
var p2Timer;
var p1Input = -1;
var p2Input = -1;
var correctAnswer;
var timer = null;

function run_game() {
    p1Timer = 10000;
    counter = 10;
    p1Score = 0;
    p2Score = -1;
    p1Input = -1;
    p2Input = -1;
    timer = null;
    $("#total-score").text(p1Score);

    $(document).keypress(function(e) {
        var key = e.which;
        console.log(key);
        if (key == 119) {
            p1Input = 1;
        }
        else if (key == 97) {
            p1Input = 2;
        }
        else if(key == 100) {
            p1Input = 3;
        }
        else if (key == 115) {
            p1Input = 4;
        }
    });
    iterate_single_game();
}

function iterate_single_game() {
    $("#questions-left").text(counter);
    if (counter == 0) {
        end_game(1);
        console.log("GAME OVER");
        return;
    }
    p1Timer = 10000;
    p1Input = -1;

    counter--;
    correctAnswer = Math.ceil(Math.random() * 4);
    execute_query(correctAnswer);

}

function insert_next_button(players) {

    if ( $( "#button-row" ).length ) {
        return;
    }

    var correctValue ='';
    if (correctAnswer == 1) {
        $('#top-container').css({'border-radius': '25px', 'border': '2px solid #73AD21'});
        correctValue = $('#top-option').text();
    } else if (correctAnswer == 2) {
        $('#left-div').css({'border-radius': '25px', 'border': '2px solid #73AD21'});
        correctValue = $('#left-option').text();
    } else if (correctAnswer == 3) {
        $('#right-div').css({'border-radius': '25px', 'border': '2px solid #73AD21'});
        correctValue = $('#right-option').text();
    } else {
        $('#bottom-container').css({'border-radius': '25px', 'border': '2px solid #73AD21'});
        correctValue = $('#bottom-option').text();
    }

    var button_element;
    if (players == 1) {
        button_element = "<div id='button-row' class='row'><div id='reset-button' class='col-sm-1'><input class='btn btn-primary' id='next-button' type='button' value='Next' onclick='remove_next_button(1);' /></div>";
    } else {
        button_element = "<div id='button-row' class='row'><div id='reset-button' class='col-sm-1'><input class='btn btn-primary' id='next-button' type='button' value='Next' onclick='remove_next_button(2);' /></div>";
    }

    var bing_search_element = "<div id='bing-search' class='col-sm-6'><div id='imaginary_container'><img src='http://www.microsoft.com/presspass/_resources/images/img_windowsBingLogo.png' alt='Bing' /></a><form method='GET' target='_blank' action='http://www.bing.com/search'> <div class='input-group stylish-input-group'><input id='bing-input' type='text' class='form-control' name='q' id='q' placeholder='Search' ><span class='input-group-addon'><button value='lookup' type='submit'><span class='glyphicon glyphicon-search'></span></button></span></div></div></div></div>";
    $('#game-row').after(button_element.concat(bing_search_element));
    var questionText = $('#question').text();
    if (questionText.indexOf("population?") != -1) {
        questionText = "population of "+ correctValue;
    } else {
        questionText = questionText + " " + correctValue;
    }
    $('#bing-input').attr('value', questionText);

}

function remove_next_button(players) {
    $('#top-container').css({'border-radius': '25px', 'border': '0px solid #73AD21'});
    $('#left-div').css({'border-radius': '25px', 'border': '0px solid #73AD21'});
    $('#right-div').css({'border-radius': '25px', 'border': '0px solid #73AD21'});
    $('#bottom-container').css({'border-radius': '25px', 'border': '0px solid #73AD21'});

    $('#button-row').remove();
    if (players == 1) {
        iterate_single_game();
    } else {
        iterate_two_player_game();
    }
}

function run_two_player_game() {
    //initialize counter
    counter = 11;
    //respond to keypress

    p1Score = 0;
    p2Score = 0;

    $("#total-score-p1").text(p1Score);
    $("#total-score-p2").text(p2Score);
    $(document).keypress(function(e) {
        var key = e.which;
        //set p1 if WASD
        if (key == 119) {
            p1Input = 1;
        }
        else if (key == 97) {
            p1Input = 2;
        }
        else if(key == 100) {
            p1Input = 3;
        }
        else if (key == 115) {
            p1Input = 4;
        }
        //set P2 if IJKL
        if (key == 105) {
            p2Input = 1;
        }
        else if (key == 106) {
            p2Input = 2;
        }
        else if(key == 108) {
            p2Input = 3;
        }
        else if (key == 107) {
            p2Input = 4;
        }
    });
    iterate_two_player_game();
}

function iterate_two_player_game() {
    $("#questions-left").text(counter - 1);
    timer = null;
    p1Timer = 10000;
    p2Timer = 10000;
    p1Input = -1;
    p2Input = -1;

    counter--;

    if (counter == 0) {
        console.log("GAME OVER");
        end_game(2);
        return;
    }

    correctAnswer = Math.ceil(Math.random() * 4);
    execute_query(correctAnswer);
}

function end_game(numPlayers) {
    $("#top-option").text("");
    $("#left-option").text("");
    $("#right-option").text("");
    $("#bottom-option").text("");
    if (p2Score <= -1) {
        $.get('/highScores1', { players: "1", p1score: p1Score });
    }
    else {
        $.get('/highScores2', 
        { 
            players: "2", p1score: "" + p1Score , p2score: p2Score , 

        });
    }

    //add a button to go back to index
    switch (numPlayers) {
        case 1:
            $("#question").text("Game Over: your score is " + p1Score);
            break;

        case 2:
            //add both players scores to highscores

            if (p1Score > p2Score) {
                $("#question").text("Game Over: Player 1 wins");
            }
            else if (p1Score < p2Score) {
                $("#question").text("Game Over: Player 2 wins");
            }
            else {
                $("#question").text("really? a tie? lame");
            }
            break;
    }
    var button_element = "<div id='button-row' class='row'><div id='reset-button' class='col-sm-3'><input class='btn btn-primary' id='reset-button' type='button' value='RESET GAME' onclick='reset_game();' /></div>";
    //var bing_search_element = "<div id='bing-search' class='col-sm-6'><div id='imaginary_container'><div class='input-group stylish-input-group'><input type='text' class='form-control'  placeholder='Search' ><span class='input-group-addon'><button type='submit'><span class='glyphicon glyphicon-search'></span></button></span></div></div></div></div>";
    $('#game-row').after(button_element);
}

function reset_game() {
    $('#button-row').remove();
    $('#reset-button').remove();
    if (p2Score <= -1) {
        run_game();
    }
    else {
        run_two_player_game();
    }
    
}

function execute_query(answerIndex) {
    $.get('/callQuery', {difficulty : "easy"}, function(resultSet) {
        $("#question").text(resultSet[0]);
        console.log("answer index is" + answerIndex);
        switch (answerIndex) {
            case 1:
                $("#top-option").text(resultSet[1]);
                $("#left-option").text(resultSet[2]);
                $("#right-option").text(resultSet[3]);
                $("#bottom-option").text(resultSet[4]);
                break;

            case 2:
                $("#top-option").text(resultSet[2]);
                $("#left-option").text(resultSet[1]);
                $("#right-option").text(resultSet[3]);
                $("#bottom-option").text(resultSet[4]);
                break;

            case 3:
                $("#top-option").text(resultSet[2]);
                $("#left-option").text(resultSet[3]);
                $("#right-option").text(resultSet[1]);
                $("#bottom-option").text(resultSet[4]);
                break;

            case 4:
                $("#top-option").text(resultSet[2]);
                $("#left-option").text(resultSet[3]);
                $("#right-option").text(resultSet[4]);
                $("#bottom-option").text(resultSet[1]);
                break;

        }
        if (timer === null) {
            if (p2Score == -1) {
                timer = setInterval ( function() {
                    if (p1Timer <= 200 || p1Input != -1) {
                                //correct answer
                        if (p1Input == correctAnswer) {
                            p1Score += p1Timer;
                            $("#total-score").text(p1Score);
                            clearInterval(timer);
                            timer = null;
                            insert_next_button(1);
                        }
                        //wrong answer but actual input
                        else if (p1Input != -1) {
                            clearInterval(timer);
                            timer=null;
                            insert_next_button(1);
                        }
                        $("#questions-left").text(counter);
                    }
                    else {
                        p1Timer -= 200;
                        $("#question-score").text(p1Timer);
                    }
                }, 200);
            }
            else {
                timer = setInterval ( function() {
                    $("#question-score-p1").text(p1Timer);
                    $("#question-score-p2").text(p2Timer);
                    if (p1Input == -1 && p1Timer >= 0) {
                        p1Timer -= 200;
                    }
                    if (p2Input == -1 && p2Timer >= 0) {
                        p2Timer -= 200;
                    }

                    //check if both players have responded
                    if ((p1Input > -1 || p1Timer < 0) && (p2Input > -1 || p2Timer < 0)) {
                        if (p1Input == correctAnswer && p1Timer > 0) {
                            p1Score += p1Timer;
                        }
                        if (p2Input == correctAnswer && p2Timer > 0) {
                            p2Score += p2Timer;
                        }
                        clearInterval(timer);
                        timer = null;
                        $("#total-score-p1").text(p1Score);
                        $("#total-score-p2").text(p2Score);
                        if (counter > 0) {
                            insert_next_button(2);
                        }
                        $("#questions-left").text(counter - 1);
                    }
                }, 200);
            }
        }
    });
}

function fillTableLabels() {

}

