exports.giveQuery = function (difficulty, db, MongoClient, res) {
    //eventually will be determined elsewhere
    var questionType;

    //return variables
    var questionString = "";
    var queryString = "";
    var correctAnswer = "";
    var wrongQueryString = "";
    var wrongAnswers = new Array("", "", "");

    // Connection URL. This is where your mongodb server is running.
    var uri = 'mongodb://cswords:cis550db@ds133388.mlab.com:33388/cis550sports_trivia';

    //if a question requires the medals table, we restrict based
    //on difficulty level
    var medalview = "";
    switch (difficulty) {
        case "easy":
            medalview = "medals_easy";
            questionType = Math.ceil(Math.random() * 7);
            break;

        case "medium":
            medalview = "medals_medium";
            questionType = Math.ceil(Math.random() * 12);
            break;

        case "hard":
            medalview = "medals";
            questionType = Math.ceil(Math.random() * 12);
            break;
    }
    //random number generated for question type
    //currently static but will eventually change based on user input

    switch(questionType) {

        //WHO WON X MEDAL FOR Y EVENT IN Z YEAR?
        case 1:

            var year;
            var medal_type;
            var event_name;

            //figure out query string
            //medalview is dependent on difficulty
            queryString = "SELECT * FROM (SELECT m.year, m.medal_type, a.athlete_name, e.event_name FROM "
                + medalview
                + " m INNER JOIN athletes a ON m.athlete_id = a.athlete_id INNER JOIN events e"
                + " ON m.event_id = e.event_id ORDER BY dbms_random.value) WHERE ROWNUM <=1";

            //perform query
            db.getConnection(function(err, conn) {
                conn.execute(queryString, [], function(err, results) {
                    if (err) {console.error(err.message); return;}
                    //process results
                    //console.log(results.rows);
                    var g = results.rows[0];
                    year = g[0];
                    medal_type = g[1];
                    correctAnswer = g[2];
                    event_name = g[3];
                    questionString = "Who won the " + medal_type + " medal for " + event_name + " in " + year + "?";

                    wrongQueryString = "SELECT * FROM (SELECT athlete_name FROM athletes WHERE athlete_name <> '"
                        + correctAnswer
                        + "' ORDER BY dbms_random.value) WHERE ROWNUM <= 3";

                    conn.execute(wrongQueryString, [], function(err, results) {
                        if (err) {console.error("wrong!!!" + wrongQueryString + err.message); return;}
                        //process results
                        //console.log(results.rows);
                        for (var i = 0; i < 3; i++) {
                            wrongAnswers[i] = results.rows[i][0];
                        }

                        conn.close();
                        var outArray = returnConstructor(questionString, correctAnswer, wrongAnswers);
                        res.send(outArray);
                    });
                });
            });
            break;



        //WHICH COUNTRY WON THE MOST MEDALS IN X YEAR?
        case 2:
            //figure out query string
            var year;
            queryString = "SELECT * FROM (SELECT year, top_medalist FROM olympic_years ORDER BY dbms_random.value) WHERE rownum <= 1"

            //perform query
            db.getConnection(function(err, conn) {
                conn.execute(queryString, [], function(err, results) {
                    if (err) {console.error("bird" + err.message); return;}
                    //process results
                    //console.log(results.rows);
                    var g = results.rows[0];
                    year = g[0];
                    correctAnswer = g[1];
                    questionString = "Which country won the most medals in " + year + "?";

                    wrongQueryString = "SELECT * FROM (SELECT country_name FROM countries WHERE country_name <> '"
                        + correctAnswer
                        + "' ORDER BY dbms_random.value) WHERE ROWNUM <= 3";


                    conn.execute(wrongQueryString, [], function(err, results) {
                        if (err) {console.error("wrong!!!" + wrongQueryString + err.message); return;}
                        //process results
                        //console.log(results.rows);
                        for (var i = 0; i < 3; i++) {
                            wrongAnswers[i] = results.rows[i][0];
                        }

                        conn.close();
                        var outArray = returnConstructor(questionString, correctAnswer, wrongAnswers);
                        res.send(outArray);
                    });
                });
            });
            break;


        //WHERE DID THE OLYMPICS TAKE PLACE IN X YEAR?
        case 3:

            var year;

            //figure out query string
            queryString = "SELECT * FROM (SELECT year, country_name FROM OLYMPIC_YEARS ORDER BY dbms_random.value) WHERE rownum <= 1";

            //perform query
            db.getConnection(function(err, conn) {
                conn.execute(queryString, [], function(err, results) {
                    if (err) {console.error("bird" + err.message); return;}
                    //process results
                    //console.log(results.rows);
                    var g = results.rows[0];
                    year = g[0];
                    correctAnswer = g[1];
                    questionString = "Where did the olympics take place in " + year + "?";

                    wrongQueryString = "SELECT * FROM (SELECT country_name FROM olympic_years WHERE country_name <> '"
                        + correctAnswer
                        + "' ORDER BY dbms_random.value) WHERE ROWNUM <= 3";


                    conn.execute(wrongQueryString, [], function(err, results) {
                        if (err) {console.error("wrong!!!" + wrongQueryString + err.message); return;}
                        //process results
                        //console.log(results.rows);
                        for (var i = 0; i < 3; i++) {
                            wrongAnswers[i] = results.rows[i][0];
                        }

                        conn.close();
                        var outArray = returnConstructor(questionString, correctAnswer, wrongAnswers);
                        //
                        res.send(outArray);
                    });
                });
            });
            break;


        //WHICH PERSON IN X EVENT HAS THE MOST TOTAL MEDALS?
        case 4:

            var athlete_name;
            var event_name;
            var medal_count;

            //figure out query string
            queryString = "SELECT * FROM (SELECT a.athlete_name, e.event_name, med.count FROM"
                + " (SELECT athlete_id as a_id, event_id as e_id, COUNT(*) as count FROM medals"
                + " GROUP BY athlete_id, event_id) med INNER JOIN (SELECT event_id as random_event FROM"
                + " (SELECT event_id FROM " + medalview + " ORDER BY dbms_random.value) WHERE rownum <=1) rand"
                + " ON med.e_id = rand.random_event INNER JOIN athletes a ON med.a_id = a.athlete_id INNER JOIN events e"
                + " ON med.e_id = e.event_id ORDER BY count desc) WHERE rownum <= 1"

            //perform query
            db.getConnection(function(err, conn) {
                conn.execute(queryString, [], function(err, results) {
                    if (err) {console.error("bird" + err.message); return;}
                    //process results
                    //console.log(results.rows);
                    var g = results.rows[0];
                    correctAnswer = g[0];
                    event_name = g[1];
                    medal_count = g[2];
                    questionString = "Which person in " + event_name + " has the most total medals (" + medal_count + ")?";

                    wrongQueryString = "SELECT * FROM (SELECT athlete_name FROM athletes WHERE athlete_name <> '"
                        + correctAnswer
                        + "' ORDER BY dbms_random.value) WHERE ROWNUM <= 3";


                    conn.execute(wrongQueryString, [], function(err, results) {
                        if (err) {console.error("wrong!!!" + wrongQueryString + err.message); return;}
                        //process results
                        //console.log(results.rows);
                        for (var i = 0; i < 3; i++) {
                            wrongAnswers[i] = results.rows[i][0];
                        }

                        conn.close();
                        var outArray = returnConstructor(questionString, correctAnswer, wrongAnswers);
                        //
                        res.send(outArray);
                    });
                });
            });
            break;



        //WHICH X-PLAYER WON Y ACCOLADE IN Z YEAR?
        case 5:
            var sportTable = "NO SPORT SELECTED YET";
            var sportPlayer = "";
            var randomNum = Math.ceil(Math.random() * 6);
            switch(randomNum) {
                case 1:
                    sportTable = "nfl_accolades";
                    sportPlayer = "football player";
                    break;

                case 2:
                    sportTable = "soccer_accolades";
                    sportPlayer = "soccer player";
                    break;

                case 3:
                    sportTable = "golf_accolades_men";
                    sportPlayer = "men's golfer";
                    break;

                case 4:
                    sportTable = "tennis_accolades_men";
                    sportPlayer = "men's tennis player";
                    break;

                case 5:
                    sportTable = "tennis_accolades_women";
                    sportPlayer = "women's tennis player";
                    break;

                case 6:
                    sportTable = "nba_accolades";
                    sportPlayer = "men's basketball player";
                    break;
            }
            //randomly generate number to decide which sport to ask about

            //figure out query string
            queryString = "SELECT * FROM (SELECT year, winner, accolade FROM " + sportTable + " ORDER BY dbms_random.value) WHERE ROWNUM <=1";

            var year;
            var winner;
            var accolade;

            //perform query
            db.getConnection(function(err, conn) {
                conn.execute(queryString, [], function(err, results) {
                    if (err) {console.error("bird" + err.message); return;}
                    //process results
                    //console.log(results.rows);
                    var g = results.rows[0];
                    year = g[0];
                    correctAnswer = g[1];
                    accolade = g[2];
                    questionString = "Which " + sportPlayer + " won the " + accolade + " in " + year + " ?";

                    wrongQueryString = "SELECT * FROM (SELECT winner FROM "
                        + sportTable
                        + " WHERE winner <> '"
                        + correctAnswer
                        + "' ORDER BY dbms_random.value) WHERE ROWNUM <= 3";

                    conn.execute(wrongQueryString, [], function(err, results) {
                        if (err) {console.error("wrong!!!" + wrongQueryString + err.message); return;}
                        //process results
                        //console.log(results.rows);
                        for (var i = 0; i < 3; i++) {
                            wrongAnswers[i] = results.rows[i][0];
                        }

                        conn.close();
                        var outArray = returnConstructor(questionString, correctAnswer, wrongAnswers);
                        //
                        res.send(outArray);
                    });
                });
            });
            break;



        //WHICH COUNTRY HAS THE LARGEST POPULATION?
        case 6:
            //figure out query string
            //medalview is dependent on difficulty
            queryString = "SELECT * FROM (SELECT country_name, population FROM countries ORDER BY dbms_random.value) WHERE ROWNUM <= 4";

            //perform query
            db.getConnection(function(err, conn) {
                conn.execute(queryString, [], function(err, results) {
                    if (err) {console.error("bird" + err.message); return;}
                    //process results
                    //console.log(results.rows);
                    var g = results.rows;

                    var country1 = results.rows[0];
                    var country2 = results.rows[1];
                    var country3 = results.rows[2];
                    var country4 = results.rows[3];

                    var maxPop = Math.max(country1[1], country2[1], country3[1], country4[1]);

                    if (maxPop == country1[1]) {
                        correctAnswer = country1[0];
                        wrongAnswers = [country2[0], country3[0], country4[0]];
                    } else if (maxPop == country2[1]) {
                        correctAnswer = country2[0];
                        wrongAnswers = [country1[0], country3[0], country4[0]];
                    } else if (maxPop == country3[1]) {
                        correctAnswer = country3[0];
                        wrongAnswers = [country1[0], country2[0], country4[0]];
                    } else if (maxPop == country4[1]) {
                        correctAnswer = country4[0];
                        wrongAnswers = [country1[0], country2[0], country3[0]];
                    }

                    questionString = "Which country has the largest population?";

                    var outArray = returnConstructor(questionString, correctAnswer, wrongAnswers);
                    
                    res.send(outArray);

                });
            });
            break;


        //RANDOM SPORTS QUESTION FROM MONGODB
        case 7:
            //figure out query string
            //medalview is dependent on difficulty

            var randomMongo = Math.ceil(Math.random() * 35) - 1;
            /*queryString = "db.questions.find({}, {question : 1, correct_answer : 1, incorrect_answers : 1, _id : 0}).limit(-1).skip("
             + randomMongo
             + ").next()";*/

            //perform query
            // Use connect method to connect to the Server
            MongoClient.connect(uri, function (err, db) {

                if (err) {
                    console.log('Unable to connect to the mongoDB server. Error:', err);
                } else {
                    //HURRAY!! We are connected. :)
                    console.log('Connection established to', uri);

                    var questions = db.collection('questions');

                    questions.find({}, {question : 1, correct_answer : 1, incorrect_answers : 1, _id : 0}).toArray(function (err, docs) {

                        if(err) throw err;

                        questionString = docs[randomMongo]['question'];
                        correctAnswer = docs[randomMongo]['correct_answer'];
                        wrongAnswers = docs[randomMongo]['incorrect_answers'];

                        var outArray = returnConstructor(questionString, correctAnswer, wrongAnswers);
                        
                        res.send(outArray);

                        db.close(function (err) {
                            if(err) throw err;
                        });
                    });
                }
            });

            break;


        //IN WHAT YEAR WAS X EVENT ADDED?
        case 8:
            var eventId = Math.ceil(Math.random() * 673);
            var eventName;
            var initialYear;

            //figure out query string
            //medalview is dependent on difficulty
            queryString = "SELECT e.event_name, randomEvent.min_year FROM (SELECT e_id, min_year FROM"
                + " (SELECT m.event_id as e_id, MIN(year) as min_year FROM medals m GROUP BY m.event_id"
                + " ORDER BY dbms_random.value) WHERE ROWNUM <=1) randomEvent INNER JOIN events e ON randomEvent.e_id = e.event_id"

            //perform query
            db.getConnection(function(err, conn) {
                conn.execute(queryString, [], function(err, results) {
                    if (err) {console.error("bird" + err.message); return;}
                    //process results
                    //console.log(results.rows);
                    var g = results.rows[0];
                    eventName = g[0];
                    correctAnswer = g[1];
                    questionString = "In what year was " + eventName + " added?";

                    wrongQueryString = "SELECT * FROM (SELECT year FROM olympic_years WHERE year <> "
                        + correctAnswer
                        + " ORDER BY dbms_random.value) WHERE ROWNUM <= 3"

                    conn.execute(wrongQueryString, [], function(err, results) {
                        if (err) {console.error("wrong!!!" + wrongQueryString + err.message); return;}
                        //process results
                        //console.log(results.rows);
                        for (var i = 0; i < 3; i++) {
                            wrongAnswers[i] = results.rows[i][0];
                        }

                        conn.close();
                        var outArray = returnConstructor(questionString, correctAnswer, wrongAnswers);
                        
                        res.send(outArray);
                    });
                });
            });
            break;



        //HOW MANY TIMES HAS X-PLAYER WON Y ACCOLADE?
        case 9:
            var sportTable = "NO SPORT SELECTED YET";
            var randomNum = Math.ceil(Math.random() * 6);
            switch(randomNum) {
                case 1:
                    sportTable = "nfl_accolades"
                    sportPlayer = "football player"
                    break;

                case 2:
                    sportTable = "soccer_accolades"
                    sportPlayer = "soccer player"
                    break;

                case 3:
                    sportTable = "golf_accolades_men"
                    sportPlayer = "men's golfer"
                    break;

                case 4:
                    sportTable = "tennis_accolades_men"
                    sportPlayer = "men's tennis player"
                    break;

                case 5:
                    sportTable = "tennis_accolades_women"
                    sportPlayer = "women's tennis player"
                    break;

                case 6:
                    sportTable = "nba_accolades"
                    sportPlayer = "men's basketball player"
                    break;
            }

            var winner;
            var accolade;
            var winCount;

            //figure out query string
            //medalview is dependent on difficulty
            queryString = "SELECT * FROM (SELECT winner, accolade, COUNT(*) FROM "
                + sportTable
                + " GROUP BY winner, accolade HAVING COUNT(*) > 1 ORDER BY dbms_random.value) WHERE ROWNUM <= 1";

            //perform query
            db.getConnection(function(err, conn) {
                conn.execute(queryString, [], function(err, results) {
                    if (err) {console.error("bird" + err.message); return;}
                    //process results
                    //console.log(results.rows);
                    var g = results.rows[0];
                    winner = g[0];
                    accolade = g[1];
                    correctAnswer = g[2];
                    questionString = "How many times has " + winner + " won the " + accolade + "?";

                    for (var i = 0; i < 3; i++) {
                        var offset = 0;
                        while (offset == 0 || wrongAnswers.indexOf(correctAnswer + offset) > -1) {
                            offset = Math.ceil(Math.random() * (correctAnswer + 3));
                            offset = offset - correctAnswer + 1;
                        }
                        wrongAnswers[i] = correctAnswer + offset;
                    }

                    conn.close();
                    var outArray = returnConstructor(questionString, correctAnswer, wrongAnswers);
                    
                    res.send(outArray);
                });
            });
            break;



        //HOW MANY MEDALS HAS X ATHLETE WON?
        case 10:
            var athlete_name;
            var medal_count;

            //figure out query string
            queryString = "SELECT a.athlete_name, randomAthlete.count FROM (SELECT * FROM"
                + " (SELECT m.athlete_id as a_id, COUNT(*) as count FROM "
                + medalview
                + " m GROUP BY m.athlete_id HAVING COUNT(*) > 4 ORDER BY dbms_random.value)"
                + " WHERE ROWNUM <= 1) randomAthlete INNER JOIN athletes a ON randomAthlete.a_id = a.athlete_id";

            //perform query
            db.getConnection(function(err, conn) {
                conn.execute(queryString, [], function(err, results) {
                    if (err) {console.error("bird" + err.message); return;}
                    //process results
                    //console.log(results.rows);
                    var g = results.rows[0];
                    athlete_name = g[0];
                    medal_count = g[1];
                    correctAnswer = medal_count;
                    questionString = "How many medals has " + athlete_name + " won?";

                    for (var i = 0; i < 3; i++) {
                        var offset = 0;
                        while (offset == 0 || wrongAnswers.indexOf(correctAnswer + offset) > -1) {
                            offset = Math.ceil(Math.random() * (correctAnswer + 3));
                            offset = offset - correctAnswer + 1;
                        }
                        wrongAnswers[i] = correctAnswer + offset;
                    }

                    conn.close();
                    var outArray = returnConstructor(questionString, correctAnswer, wrongAnswers);
                    
                    res.send(outArray);
                });
            });
            break;



        //WHAT IS THE MOST RECENT OLYMPICS IN WHICH ATHLETE X WON A MEDAL?
        case 11:
            var athlete_name;

            //figure out query string
            queryString = "SELECT a.athlete_name, year FROM (SELECT a_id, year FROM (SELECT m.athlete_id as a_id, MAX(m.year) as year FROM "
                + medalview
                + " m GROUP BY m.athlete_id ORDER BY dbms_random.value) WHERE ROWNUM <= 1) INNER JOIN athletes a ON a_id = a.athlete_id";

            //perform query
            db.getConnection(function(err, conn) {
                conn.execute(queryString, [], function(err, results) {
                    if (err) {console.error(err.message); return;}
                    //process results
                    //console.log(results.rows);
                    var g = results.rows[0];
                    athlete_name = g[0];
                    correctAnswer = g[1];
                    questionString = "What is the most recent olympics in which " + athlete_name + " won a medal?";

                    wrongQueryString = "SELECT * FROM (SELECT year FROM olympic_years WHERE year <> "
                        + correctAnswer
                        + " ORDER BY dbms_random.value) WHERE ROWNUM <= 3"

                    conn.execute(wrongQueryString, [], function(err, results) {
                        if (err) {console.error("wrong!!!" + wrongQueryString + err.message); return;}
                        //process results
                        //console.log(results.rows);
                        for (var i = 0; i < 3; i++) {
                            wrongAnswers[i] = results.rows[i][0];
                        }

                        conn.close();
                        var outArray = returnConstructor(questionString, correctAnswer, wrongAnswers);
                        
                        res.send(outArray);
                    });
                });
            });
            break;


        //WHICH COUNTRY WON THE MOST GOLD MEDALS IN X YEAR?
        case 12:
            //figure out query string
            var year;
            queryString = "SELECT * FROM (SELECT year, top_gold_medalist FROM olympic_years ORDER BY dbms_random.value) WHERE rownum <= 1"

            //perform query
            db.getConnection(function(err, conn) {
                conn.execute(queryString, [], function(err, results) {
                    if (err) {console.error("bird" + err.message); return;}
                    //process results
                    var g = results.rows[0];
                    year = g[0];
                    correctAnswer = g[1];
                    questionString = "Which country won the most gold medals in " + year + "?";

                    wrongQueryString = "SELECT * FROM (SELECT country_name FROM countries WHERE country_name <> '"
                        + correctAnswer
                        + "' ORDER BY dbms_random.value) WHERE ROWNUM <= 3";


                    conn.execute(wrongQueryString, [], function(err, results) {
                        if (err) {console.error("wrong!!!" + wrongQueryString + err.message); return;}
                        //process results
                        //console.log(results.rows);
                        for (var i = 0; i < 3; i++) {
                            wrongAnswers[i] = results.rows[i][0];
                        }

                        conn.close();
                        var outArray = returnConstructor(questionString, correctAnswer, wrongAnswers);
                        
                        res.send(outArray);
                    });
                });
            });
            break;

    }
}


//FUNCTION THAT FORMATS RETURN VARIABLES
function returnConstructor (question, correct, wrong) {

    return new Array(question, correct, wrong[0], wrong[1], wrong[2]);

}


